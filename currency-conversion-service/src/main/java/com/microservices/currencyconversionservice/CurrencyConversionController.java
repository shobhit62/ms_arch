package com.microservices.currencyconversionservice;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@RestController
public class CurrencyConversionController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    CurrencyExchangeServiceProxy currencyExchangeServiceProxy;
    @GetMapping("/currency-converter/from/{from}/to/{to}/quantity/{quantity}")
    @HystrixCommand(fallbackMethod = "fallback_hello" , commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
    })
    public CurrencyConversionBean convertCurrency(@PathVariable String from ,@PathVariable String to ,@PathVariable BigDecimal quantity ){
        Map<String,String> uriVariable = new HashMap<>();
        uriVariable.put("from",from);
        uriVariable.put("to",to);
        ResponseEntity<CurrencyConversionBean> responseEntity= new RestTemplate().getForEntity("http://localhost:8001/currency-exchange/from/{from}/to/{to}" , CurrencyConversionBean.class,uriVariable);
        CurrencyConversionBean response = responseEntity.getBody();
        return new CurrencyConversionBean(response.getId(),from ,to,response.getConversionMultiple(),quantity,quantity.multiply(response.getConversionMultiple()),response.getPort());
    }


    @GetMapping("/currency-converter-feign/from/{from}/to/{to}/quantity/{quantity}")
    @HystrixCommand(fallbackMethod = "fallback_hello" , commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
    })
    public CurrencyConversionBean convertCurrencyFeign(@PathVariable String from ,@PathVariable String to ,@PathVariable BigDecimal quantity ){
         CurrencyConversionBean response = currencyExchangeServiceProxy.retrieveExchangeValue(from,to);
         logger.info("CurrencyCoversionController -> {}",response);
         return new CurrencyConversionBean(response.getId(),from ,to,response.getConversionMultiple(),quantity,quantity.multiply(response.getConversionMultiple()),response.getPort());
    }

    private CurrencyConversionBean fallback_hello(String from , String to ,BigDecimal quantity) {
        CurrencyConversionBean currencyConversionBean = new CurrencyConversionBean(0,"NA","NA",BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO,0);
        return currencyConversionBean;
    }
}
