package com.microservices.currencyexchangeservice;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class CurrencyExchangeController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private Environment environment;

    @Autowired
    private ExchaqngeValueRepository repository;

    @GetMapping("/currency-exchange/from/{from}/to/{to}")
    @HystrixCommand(fallbackMethod = "fallback_hello" , commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
    })
    public ExchangeValue retrieveExchangevalue(@PathVariable String from , @PathVariable String to)
    {
        ExchangeValue exchangeValue = repository.findByFromAndTo(from,to);
        exchangeValue.setPort(Integer.parseInt(environment.getProperty("local.server.port")));
        logger.info("CurrencyExchangeController -> {}",exchangeValue);
        return exchangeValue;
    }
    private ExchangeValue fallback_hello(String from , String to) {
        ExchangeValue exchangeValue = new ExchangeValue(0,"NA","NA",BigDecimal.ZERO);
        return exchangeValue;
    }

}
