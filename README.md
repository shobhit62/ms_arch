# README #

### What is this repository for? ###

it's a basic project to demonstrate the microservice architecture . We used 
1) Spring boot for createing the microservices
2) ribbon for load balancing
3) feign for communicating between the two services 
4) eureka naming serrver for service discovery  
5) spring cloud sleuth , rabbitMq and zipkin for tracing 
6) ELK stack for log monitoring 
7) Spring boot admin for application monitoring 
8) hystrix for Tolerance and api monitoring 
9) netflix zool for api gateway

### How do I get set up? ###

1) clean and compile all the projects cloned from the repository
2) install [rabbitMQ](https://www.rabbitmq.com/install-homebrew.html) on your system if not present .
3) run the spring-boot-admin project then netflix-eureka-naming-server
4) run the rabbitMQ server
5) start the Zipkin server using the command RABBIT_URI=amqp://localhost java -jar zipkin-server-2.12.9-exec.jar . It will start the zipkin and configure it to listen to the rabbitMQ  for the messages
6) run the remaining projects currency-conversion-service , currency-exchange-service , netflix-zuul-api-gateway-server
7) then install run and configure the ELK stack using the [link](https://www.javainuse.com/spring/springboot-microservice-elk)

if you are familiar  with docker it is much easy to run the whole setup each project have the Dockerfile in them either you can use them or configure according to your Requirements 
the steps are 

1) build the docker images of the projects (currency-conversion-service , currency-exchange-service , netflix-zuul-api-gateway-server , spring-boot-admin . Zipkin not required)
2) update the image name in the docker-compose.yml present in docker files folder 
3) run the docker compose
4) for elk stack rename the docker-compose2.yml to docker-compose.yml and run it . I used volumes to share the logs betwen the microservices

### future updates ###

i will push the docker images to docker hub and update the docker compose accordingly 